#include <cstdio>
#include <unistd.h>
#include <omp.h>

int main(int argc, char **argv) {
  int pid = getpid();
  printf("[ HEAD ] pid = %d @ %p\n", pid, &pid);

  #pragma omp parallel
  #pragma omp single
  {
    #pragma omp target nowait depend(in: pid)
    {
      printf("[WORKER] pid = %d (head pid %d @ %p)\n", getpid(),
             pid, &pid);
    }
  }

  return 0;
}
